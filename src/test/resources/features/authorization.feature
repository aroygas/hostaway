Feature: Authorization of users on a login page
  In order to authorize in the system
  As a user
  I need be able to log in and log out of the system

  @smoke
  Scenario: 1) Log in with valid credentials
    Given I open home page
    When I enter "demo@hostaway.com" login
    And I enter "1qaz2wsx3edc" password
    And I press "Login" button
    Then I should see text "Michael Piterson"

  @smoke
  Scenario: 2) Try to log in with invalid credentials
    Given I open home page
    When I enter "demo@hostaway.com" login
    And I enter "InterviewTest18" password
    And I press "Login" button
    Then I should not see text "Michael Piterson"