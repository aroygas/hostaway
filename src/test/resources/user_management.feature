Feature: User Management
  In order to manage users
  As an administrator
  I need be able to create, use, change and delete users

  @smoke
  Scenario: 1) Create admin using template
    Given I open home page
    When I enter "demo@hostaway.com" login
    And I enter "InterviewTest18" password
    And I press "Log in" button
    And I go to "User Management" tab
    And I go to "Add new" link
    And I enter "qatestexample2018@gmail.com" email
    And I enter "qwerty123456" password
    And I enter "QA TEST ALEX" first name
    And I set "Admin" permission template
    And I switch "User receives reservation notification via email" check box on
    And I switch "Allow access to contact data" check box on
    And I switch "Allow access to financial data" check box on
    And I press "Save" button
    And I press "Create user" button
    And I go to "Overview" tab
    And I go to "User Management" tab
    Then I should see user with email "qatestexample2018@gmail.com" in users list

  @smoke
  Scenario: 2) Create admin using check box
    Given I open home page
    When I enter "demo@hostaway.com" login
    And I enter "InterviewTest18" password
    And I press "Log in" button
    And I go to "User Management" tab
    And I go to "Add new" link
    And I enter "qatestexample20182@gmail.com" email
    And I enter "qwerty123456" password
    And I enter "QA TEST ALEX" first name
    And I switch "User has admin-level access" check box on
    And I switch "User receives reservation notification via email" check box on
    And I set "View" permission for "Listing" tab
    And I press "Save" button
    And I press "Create user" button
    And I go to "Overview" tab
    And I go to "User Management" tab
    Then I should see user with email "qatestexample20182@gmail.com" in users list

  @smoke
  Scenario: 2.2) Login as created admin user
    Given I open home page
    When I enter "qatestexample20182@gmail.com" login
    And I enter "qwerty123456" password
    And I press "Log in" button
    Then I should see text "Overview"
    And I should see text "Calendar"
    And I should see text "Reservations"
    And I should see text "Tasks"
    And I should see text "Messages"
    And I should see text "Listings"
    And I should see text "Website"
    And I should see text "Revenue Management"
    And I should see text "User Management"
    And I should see text "Channel Manager"
    And I should see text "Guest invoicing"
    And I should see text "Support"

  @smoke
  Scenario: 3) Edit user
    Given I open home page
    When I enter "demo@hostaway.com" login
    And I enter "InterviewTest18" password
    And I press "Log in" button
    And I go to "User Management" tab
    And I open user with "qatestexample20182@gmail.com" email for editing
    And I set "View" permission for "Reservation" tab
    And I switch "User has admin-level access" check box off
    And I switch "Allow access to financial data" check box off
    And I press "Save" button
    Then I should see text "Edit"

  @smoke
  Scenario: 3.2) Login as edited user with limited permissions
    Given I open home page
    When I enter "qatestexample20182@gmail.com" login
    And I enter "qwerty123456" password
    And I press "Log in" button
    Then I should see text "Overview"
    And I should not see text "Calendar"
    And I should see text "Reservations"
    And I should not see text "Tasks"
    And I should not see text "Messages"
    And I should see text "Listings"
    And I should not see text "Website"
    And I should not see text "Revenue Management"
    And I should not see text "User Management"
    And I should not see text "Channel Manager"
    And I should not see text "Guest invoicing"
    And I should not see text "Settings"
    And I should see text "Support"

  @smoke
  @clear
  Scenario: 4) Delete created users
    Given I open home page
    When I enter "demo@hostaway.com" login
    And I enter "InterviewTest18" password
    And I press "Log in" button
    And I go to "User Management" tab
    And I delete user with "qatestexample2018@gmail.com" email
    And I delete user with "qatestexample20182@gmail.com" email
    And I should not see user with email "qatestexample2018@gmail.com" in users list