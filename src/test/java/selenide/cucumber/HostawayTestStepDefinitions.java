package selenide.cucumber;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.qameta.allure.selenide.AllureSelenide;
import org.openqa.selenium.By;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import utils.configReader;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import static com.codeborne.selenide.Condition.appears;
import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Selenide.*;

public class HostawayTestStepDefinitions {
    //Default values
    Properties properties = null;
    String homepage;

    //Cucumber Before
    @Before
    public void setUp() {
        //Read properties
        try {
            properties = new configReader().getPropValues();
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        }
        homepage = properties.getProperty("homepage");

        //Set up Browser
        Configuration.browser = "chrome";
        Configuration.startMaximized = false;
        Configuration.reportsFolder = "build/allure-results";
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--lang=en-US");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
        Configuration.browserCapabilities = capabilities;

        //AddListener
        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true).savePageSource(false));
    }

    //Cucumber After
    @After
    public void tearDown() {
        clearBrowserLocalStorage();
        close();
    }

    @Given("I open \"([^\"]*)\" url")
    public void openPage(String relativeOrAbsoluteUrl) {
        open(relativeOrAbsoluteUrl);
    }

    @Given("I open home page")
    public void openGiftPage() {
        this.openPage(homepage);
    }

    @When("I press \"([^\"]*)\" button")
    public void pressButton(String buttonText) {
        $(By.xpath("//button/*[contains(text(), '" + buttonText +"')]|//button[contains(text(), '" + buttonText + "')]")).click();
    }

    @When("I enter \"([^\"]*)\" (login|email)")
    public void enterLogin(String login, String field) {
        $(By.name("email")).val(login);
    }

    @When("I enter \"([^\"]*)\" password")
    public void enterPassword(String password) {
        $(By.name("password")).val(password);
    }

    @When("I enter \"([^\"]*)\" first name")
    public void enterFirstName(String password) {
        $(By.name("firstName")).val(password);
    }

    @When("I switch \"([^\"]*)\" check box ([^\"]*)")
    public void setCheckBox(String checkBoxName, String desiredState) {
        String currentState = $(By.xpath("//label[contains(text(), '" + checkBoxName + "')]/following-sibling::label[contains(@class, 'checkbox-label-container')]//input")).getValue();
        $(By.xpath("//label[contains(text(), '" + checkBoxName + "')]/following-sibling::label[contains(@class, 'checkbox-label-container')]")).click();
        $(By.xpath("//label[contains(text(), '" + checkBoxName + "')]/following-sibling::label[contains(@class, 'checkbox-label-container')]")).click();
        if ((desiredState.equals("on") && currentState.equals("false"))||(desiredState.equals("off") && currentState.equals("true"))) {
            $(By.xpath("//label[contains(text(), '" + checkBoxName + "')]/following-sibling::label[contains(@class, 'checkbox-label-container')]")).click();
        }
    }

    @When("I set \"([^\"]*)\" permission for \"([^\"]*)\" tab")
    public void setPermission(String permission, String tabName) {
        String tdNumber="1";
        if (permission.equals("Modify")) {
            tdNumber="2";
        } else if (permission.equals("Create")) {
            tdNumber="3";
        } else if (permission.equals("Delete")) {
            tdNumber="4";
        }
        $(By.xpath("//td[contains(text(), '" + tabName + "')]/following-sibling::td[" + tdNumber + "]//label[contains(@class, 'checkbox-label-container')]")).click();
    }

    @When("I set \"([^\"]*)\" permission template")
    public void setPermissionTemplate(String permissionTemplateName) {
        $(By.xpath("//label[contains(text(), 'Permission Template')]/following-sibling::select[@name='template']")).click();
        $(By.xpath("//label[contains(text(), 'Permission Template')]/following-sibling::select[@name='template']/option[text()='" + permissionTemplateName + "']")).click();
    }
    //label[contains(text(), 'Permission Template')]/following-sibling::select[@name='template']

    @When("I go to \"([^\"]*)\" (tab|link)")
    public void goToTab(String tabName, String field) {
        $(By.xpath("//a/*[contains(text(), '" + tabName +"')]")).click();
    }

    @When("I delete user with \"([^\"]*)\" email")
    public void deleteUserWithEmail(String email) {
        $(By.xpath("//tr/td[contains(text(), '" + email + "')]/following-sibling::td//button[contains(., 'Delete')]")).click();
        $(By.xpath("//a[contains(text(), 'Confirm')]")).click();
    }

    @When("I open user with \"([^\"]*)\" email for editing")
    public void openUserWithEmailForEditing(String email) {
        $(By.xpath("//tr/td[contains(text(), '" + email + "')]/following-sibling::td//a[contains(., 'Edit')]")).click();
    }

    @Then("I should ([^\"]*) user with email \"([^\"]*)\" in users list")
    public void shouldSeeUserWithEmail(String shouldSee, String email) {
        if (shouldSee.equals("see")) {
            $(By.xpath("//tr/td[contains(text(), '" + email + "')]")).shouldBe(appears);
        } else {
            $(By.xpath("//tr/td[contains(text(), '" + email + "')]")).shouldBe(disappear);
        }
    }

    @Then("I should ([^\"]*) text \"([^\"]*)\"(?:| in ([^\"]*))")
    public void shouldSee(String shouldSee, String text, String element) {
        if (element == null){
            element = "*";
        }
        if (shouldSee.equals("see")) {
            $(By.xpath("//" + element + "[contains(text(), \"" + text + "\")]")).shouldBe(appears);
        } else {
            $(By.xpath("//" + element + "[contains(text(), \"" + text + "\")]")).shouldBe(disappear);
        }
    }
}
