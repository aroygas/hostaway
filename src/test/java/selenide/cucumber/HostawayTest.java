package selenide.cucumber;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.junit.TextReport;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestRule;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        glue = "selenide.cucumber",
        tags = "@smoke",
        dryRun = false,
        strict = false,
        snippets = SnippetType.UNDERSCORE
)
public class HostawayTest {
    //@Rule
    //public TestRule report = new TextReport().onFailedTest(true).onSucceededTest(true);
}
