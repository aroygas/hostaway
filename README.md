**Test challenge for Hostaway based on Selenide + Cucumber + JUnit + Maven**

---

# Set up #

1. Install Git 
2. Install Java 11
3. Install Maven
4. Install Chrome browser

## 1. Clone this repository

 You can use these steps to clone this repo from SourceTree.

1. You�ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you�d like to and then click **Clone**.
4. Open the directory you just created to see your repository�s files.

OR

 You can clone this repo using git console.

```
git clone https://aroygas@bitbucket.org/aroygas/hostaway.git
```


# Running tests #

To start tests run this command in project folder:

```
mvn test
```

